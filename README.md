# Contributors
Submission by Christian Wu (s194597) (group 36) and Frederik Barba (s194619) (group 58). In the previous assignments we were in different groups, but we have made the final assignment together.  

# About
The executable of our simulator is called "simulator" which is in the root folder.

We have thoroughly explained our code in our report.

Our application takes 2 arguments. The first argument is the file path of the binary file that is the program the simulator should run. The second argument is the output path of the register dump.

For instance, to run tests/task/t1 one could write:

```
./simulator tests/task4/t1.bin  dumps/reg
```