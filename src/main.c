#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "inst.h"

#define MEM_SIZE 20000000
#define REG_SIZE 32

//#define INITIAL_STACK_POINTER_VALUE 2147483632
//#define INITIAL_GLOBAL_POINTER_VALUE 268435456

u_int32_t pc;
int32_t reg[REG_SIZE];
u_int8_t mem[MEM_SIZE];

int is_running = 1;
int exit_code = 0;

/**
 * @brief Throw error.
 *
 * @param msg Error message.
 */
void fail_with(char *msg)
{
    printf("Error: %s \n", msg);
    exit(1);
}

/**
 * @brief Halts the simulation.
 */
void halt_sim(int code)
{
    is_running = 0;
    exit_code = code;
}

/**
 * @brief Writes to the register.
 */
void reg_write(size_t reg_num, int32_t value)
{
    // x0 (reg[0]) is a readonly register.
    if (reg_num != 0)
        reg[reg_num] = value;
}

/**
 * @brief Reads the content of the register as a signed int.
 */
int32_t reg_read(size_t reg_num)
{
    return reg[reg_num];
}

/**
 * @brief Reads the content of the register as an unsigned int.
 */
u_int32_t u_reg_read(size_t reg_num)
{
    return (int32_t)reg[reg_num];
}

/**
 * @brief Checks the condition for jumping and jump if it is fulfilled.
 */
int try_jump(int condition, int32_t offset)
{
    if (condition)
    {
        pc += offset;
        return 1;
    }
    return 0;
}

/**
 * @brief Gets byte from the memory.
 */
int8_t *mem_b(int addr)
{
    return (int8_t *)(mem + addr);
}

/**
 * @brief Gets half-word from the memory.
 */
int16_t *mem_h(int addr)
{
    return (int16_t *)(mem + addr);
}

/**
 * @brief Gets word from the memory.
 */
int32_t *mem_w(int addr)
{
    return (int32_t *)(mem + addr);
}

/**
 * @brief Sign extends the value.
 */
int32_t sign_ex(u_int32_t num, unsigned char num_bit_count)
{
    int s = 32 - num_bit_count;
    return (((int32_t)num) << s) >> s;
}

/**
 * @brief Executes an instruction.
 * @return 1 if the program counter should not be incremented this cycle.
 */
int execute(u_int32_t inst)
{
    // Fields of the possible instruction formats.
    u_int32_t opcode = (inst)&0b1111111;
    u_int32_t rd = (inst >> 7) & 0b11111;
    u_int32_t funct3 = (inst >> 12) & 0b111;
    u_int32_t funct7 = (inst >> 25) & 0b1111111;

    u_int32_t rs1 = (inst >> 15) & 0b11111;
    u_int32_t rs2 = (inst >> 20) & 0b11111;

    u_int32_t imm110_I = (inst >> 20) & 0b111111111111;
    u_int32_t imm115_I = (inst >> 25) & 0b1111111;
    u_int32_t imm40_I = (inst >> 20) & 0b11111;

    u_int32_t imm40_S = (inst >> 7) & 0b11111;
    u_int32_t imm115_S = (inst >> 25) & 0b1111111;

    u_int32_t imm3112_U = (inst >> 12) & 0b11111111111111111111;

    u_int32_t imm12_B = (inst >> 31) & 0b1;
    u_int32_t imm105_B = (inst >> 25) & 0b111111;
    u_int32_t imm41_B = (inst >> 8) & 0b1111;
    u_int32_t imm11_B = (inst >> 7) & 0b1;

    u_int32_t imm11_J = (inst >> 20) & 0b1;
    u_int32_t imm1912_J = (inst >> 12) & 0b11111111;
    u_int32_t imm101_J = (inst >> 21) & 0b1111111111;
    u_int32_t imm20_J = (inst >> 31) & 0b1;

    // Values used in the instructions:
    int32_t ui_immediate = sign_ex(imm3112_U, 20);
    int32_t jal_offset = sign_ex((imm101_J << 1) | (imm11_J << 11) | (imm1912_J << 12) | (imm20_J << 20), 21);
    int32_t jalr_offset = sign_ex(imm110_I, 12);
    int32_t branch_offset = sign_ex((imm41_B << 1) | (imm105_B << 5) | (imm11_B << 11) | (imm12_B << 12), 13);
    int32_t load_offset = sign_ex(imm110_I, 12);
    int32_t store_offset = sign_ex((imm115_S << 5) | (imm40_S), 12);
    int32_t alu_immediate = sign_ex(imm110_I, 12);
    int32_t shift_immediate = sign_ex(imm40_I, 5);

    // LUI
    if (opcode == OPCODE_LUI)
    {
        reg_write(rd, imm3112_U << 12);
        return 0;
    }

    // AUIPC
    if (opcode == OPCODE_AUIPC)
    {
        reg_write(rd, (imm3112_U << 12) + pc);
        return 0;
    }

    // JAL
    if (opcode == OPCODE_JAL)
    {

        reg_write(rd, pc + 4);
        pc += jal_offset;
        return 1;
    }

    // JALR
    if (opcode == OPCODE_JALR && funct3 == FUNCT3_JALR)
    {
        reg_write(rd, pc + 4);
        pc = (reg_read(rs1) + jalr_offset) & ~1;
        return 1;
    }

    // BEQ
    if (opcode == OPCODE_BEQ && funct3 == FUNCT3_BEQ)
    {
        return try_jump(reg_read(rs1) == reg_read(rs2), branch_offset);
    }

    // BNE
    if (opcode == OPCODE_BNE && funct3 == FUNCT3_BNE)
    {
        return try_jump(reg_read(rs1) != reg_read(rs2), branch_offset);
    }

    // BLT
    if (opcode == OPCODE_BLT && funct3 == FUNCT3_BLT)
    {
        return try_jump(reg_read(rs1) < reg_read(rs2), branch_offset);
    }

    // BGE
    if (opcode == OPCODE_BGE && funct3 == FUNCT3_BGE)
    {
        return try_jump(reg_read(rs1) >= reg_read(rs2), branch_offset);
    }

    // BLTU
    if (opcode == OPCODE_BLTU && funct3 == FUNCT3_BLTU)
    {
        return try_jump(u_reg_read(rs1) < u_reg_read(rs1), branch_offset);
    }

    // BGEU
    if (opcode == OPCODE_BGEU && funct3 == FUNCT3_BGEU)
    {
        return try_jump(u_reg_read(rs1) >= u_reg_read(rs1), branch_offset);
    }

    // LB
    if (opcode == OPCODE_LB && funct3 == FUNCT3_LB)
    {
        reg_write(rd, *(mem_b(reg_read(rs1) + load_offset)));
        return 0;
    }

    // LH
    if (opcode == OPCODE_LH && funct3 == FUNCT3_LH)
    {
        reg_write(rd, *(mem_h(reg_read(rs1) + load_offset)));
        return 0;
    }

    // LW
    if (opcode == OPCODE_LW && funct3 == FUNCT3_LW)
    {
        reg_write(rd, *(mem_w(reg_read(rs1) + load_offset)));
        return 0;
    }

    // LBU
    if (opcode == OPCODE_LBU && funct3 == FUNCT3_LBU)
    {
        reg_write(rd, *((u_int8_t *)mem_b(reg_read(rs1) + load_offset)));
        return 0;
    }

    // LHU
    if (opcode == OPCODE_LHU && funct3 == FUNCT3_LHU)
    {
        reg_write(rd, *((u_int16_t *)mem_h(reg_read(rs1) + load_offset)));
        return 0;
    }

    // SB
    if (opcode == OPCODE_SB && funct3 == FUNCT3_SB)
    {
        *mem_b(reg_read(rs1) + store_offset) = (u_int8_t)u_reg_read(rs2);
        return 0;
    }

    // SH
    if (opcode == OPCODE_SH && funct3 == FUNCT3_SH)
    {
        *mem_h(reg_read(rs1) + store_offset) = (u_int16_t)u_reg_read(rs2);
        return 0;
    }

    // SW
    if (opcode == OPCODE_SW && funct3 == FUNCT3_SW)
    {
        *mem_w(reg_read(rs1) + store_offset) = (u_int32_t)u_reg_read(rs2);
        return 0;
    }

    // ADDI
    if (opcode == OPCODE_ADDI && funct3 == FUNCT3_ADDI)
    {
        reg_write(rd, reg_read(rs1) + alu_immediate);
        return 0;
    }

    // SLTI
    if (opcode == OPCODE_SLTI && funct3 == FUNCT3_SLTI)
    {
        if (reg_read(rs1) < alu_immediate) // HERE
        {
            reg_write(rd, 1);
        }
        else
        {
            reg_write(rd, 0);
        }

        return 0;
    }

    // SLTIU
    if (opcode == OPCODE_SLTIU && funct3 == FUNCT3_SLTIU)
    {
        if (u_reg_read(rs1) < (u_int32_t)alu_immediate || reg_read(rs1) == 0)
        {
            reg_write(rd, 1);
        }
        else
        {
            reg_write(rd, 0);
        }
        return 0;
    }

    // XORI
    if (opcode == OPCODE_XORI && funct3 == FUNCT3_XORI)
    {
        reg_write(rd, (reg_read(rs1)) ^ alu_immediate);
        return 0;
    }

    // ORI
    if (opcode == OPCODE_ORI && funct3 == FUNCT3_ORI)
    {
        reg_write(rd, (reg_read(rs1) | alu_immediate));
        return 0;
    }

    // ANDI
    if (opcode == OPCODE_ANDI && funct3 == FUNCT3_ANDI)
    {
        reg_write(rd, (reg_read(rs1)) & alu_immediate);
        return 0;
    }

    // SLLI
    if (opcode == OPCODE_SLLI && funct3 == FUNCT3_SLLI && funct7 == FUNCT7_SLLI)
    {
        reg_write(rd, (u_reg_read(rs1)) << (shift_immediate));
        return 0;
    }

    // SRLI
    if (opcode == OPCODE_SRLI && funct3 == FUNCT3_SRLI && funct7 == FUNCT7_SRLI)
    {
        reg_write(rd, (u_reg_read(rs1)) >> (shift_immediate));
        return 0;
    }

    // SRAI
    if (opcode == OPCODE_SRAI && funct3 == FUNCT3_SRAI && funct7 == FUNCT7_SRAI)
    {
        reg_write(rd, (reg_read(rs1)) >> (shift_immediate));
        return 0;
    }

    // ADD
    if (opcode == OPCODE_ADD && funct3 == FUNCT3_ADD && funct7 == FUNCT7_ADD)
    {
        reg_write(rd, reg_read(rs1) + reg_read(rs2));
        return 0;
    }

    // SUB
    if (opcode == OPCODE_SUB && funct3 == FUNCT3_SUB && funct7 == FUNCT7_SUB)
    {
        reg_write(rd, reg_read(rs1) - reg_read(rs2));
        return 0;
    }

    // SLL
    if (opcode == OPCODE_SLL && funct3 == FUNCT3_SLL && funct7 == FUNCT7_SLL)
    {
        reg_write(rd, reg_read(rs1) << (reg_read(rs2) & 0b11111));
        return 0;
    }

    // SLT
    if (opcode == OPCODE_SLT && funct3 == FUNCT3_SLT && funct7 == FUNCT7_SLT)
    {
        if (reg_read(rs1) < reg_read(rs2))
        {
            reg_write(rd, 1);
        }
        else
        {
            reg_write(rd, 0);
        }

        return 0;
    }

    // SLTU
    if (opcode == OPCODE_SLTU && funct3 == FUNCT3_SLTU && funct7 == FUNCT7_SLTU)
    {
        if (reg_read(rs1) < reg_read(rs2) || reg_read(rs2) != 0)
        {
            reg_write(rd, 1);
        }
        else
        {
            reg_write(rd, 0);
        }

        return 0;
    }

    // XOR
    if (opcode == OPCODE_XOR && funct3 == FUNCT3_XOR && funct7 == FUNCT7_XOR)
    {
        reg_write(rd, reg_read(rs1) ^ reg_read(rs2));
        return 0;
    }

    // SRL
    if (opcode == OPCODE_SRL && funct3 == FUNCT3_SRL && funct7 == FUNCT7_SRL)
    {
        reg_write(rd, u_reg_read(rs1) >> (u_reg_read(rs2) & 0b11111));
        return 0;
    }

    // SRA
    if (opcode == OPCODE_SRA && funct3 == FUNCT3_SRA && funct7 == FUNCT7_SRA)
    {
        reg_write(rd, reg_read(rs1) >> (reg_read(rs2) & 0b11111));
        return 0;
    }

    // OR
    if (opcode == OPCODE_OR && funct3 == FUNCT3_OR && funct7 == FUNCT7_OR)
    {
        reg_write(rd, reg_read(rs1) | reg_read(rs2));
        return 0;
    }

    // AND
    if (opcode == OPCODE_AND && funct3 == FUNCT3_AND && funct7 == FUNCT7_AND)
    {
        reg_write(rd, reg_read(rs1) & reg_read(rs2));
        return 0;
    }

    // ECALL
    if (opcode == ECALL)
    {
        size_t a0 = 10;
        size_t a7 = 17;
        u_int32_t command = u_reg_read(a7);
        u_int32_t value = u_reg_read(a0);

        char value_s[33];
        sprintf(value_s, "%d", value);
        double value_f = (double)value;

        switch (command)
        {
        case 1:
            printf("The value located in a0 as a signed integer %d", (int)value);
            break;
        case 2:
            printf("The value located in a0 as a float point numver is %f", value_f);
            break;
        case 4:
            printf("The null-terminated string located in address a0 is %s", value_s);
            break;
        case 10:
            halt_sim(0);
            break;
        case 11:
            printf("The value located in a0 as an ASCII character is %c", value);
            break;
        case 34:
            printf("The value located in a0 as a hex number is %x", value);
            break;
        case 35:;
            printf("The value located in a0 as a binary number is %x", value);
            for (unsigned i = 1 << 31; i > 0; i = i / 2)
            {
                (value & i) ? printf("1") : printf("0");
            }
            break;
        case 36:
            printf("The value located in a0 as an unsigned integer is %u", value);
            break;
        case 93:
            halt_sim(value);
            return 0;
        default:
            break;
        }
        return 0;
    }

    fail_with("Invalid instruction.");
    return 0;
}

/**
 * @brief Prints the contents of the registers.
 */
void print_reg(int32_t *r, size_t size)
{
    for (int i = 0; i < size; i++)
        printf("x%lu: %d \n", (unsigned long)i, r[i]);
}

void print_mem(int from, int to)
{
    for (int i = from; i < to; i++)
        printf("mem[%lu]: %d \n", (unsigned long)i, mem[i]);
}

/**
 * @brief Creates a binary dump of the contents of the registers.
 */
void dump(char *path)
{
    printf("Creating dump.\n");
    FILE *file = fopen(path, "wb");
    if (file == NULL)
    {
        fail_with("File is NULL.");
    }
    fwrite(reg, sizeof(u_int32_t), REG_SIZE, file);
}

/**
 * @brief Loads a register dump.
 */
size_t load_dump(int32_t *dump, size_t dump_size, char *path)
{
    printf("Reading file. \n");
    FILE *file = fopen(path, "rb");
    if (file == NULL)
    {
        fail_with("File not found.");
        return 0;
    }
    return fread(dump, sizeof(int32_t), dump_size, file);
}

// Loads a program for a file.
size_t load_pg(u_int8_t *buf, size_t buf_size, char *path)
{
    printf("Reading file. \n");
    FILE *file = fopen(path, "rb");
    if (file == NULL)
    {
        fail_with("File not found.");
        return 0;
    }
    return fread(buf, sizeof(u_int8_t), buf_size, file);
}

// Resets the registers and the memory by setting everything to zero.
void reset()
{
    pc = 0;
    for (int i = 0; i < REG_SIZE; i++)
        reg[i] = 0;
    for (int i = 0; i < MEM_SIZE; i++)
        mem[i] = 0;

    // reg[2] = INITIAL_STACK_POINTER_VALUE;
    // reg[3] = INITIAL_GLOBAL_POINTER_VALUE;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
        fail_with("Invalid number of arguments.");

    char *input_path = argv[1];
    char *output_path = argv[2];

    reset();

    size_t pg_size = load_pg(mem, MEM_SIZE, input_path);

    printf("Starting Execution.\n");

    while (is_running && pc < pg_size)
    {
        if (pc > MEM_SIZE)
            fail_with("PC out of bounds");

        u_int32_t inst = *((u_int32_t *)(&mem[pc]));

        //Step-wise execution.
        //printf("Program counter: %d\n", pc);
        //print_reg(reg, REG_SIZE);
        //getchar();
        
        //execute returns 1 on a sucessful branch and 0 otherwise
        //after a sucessful branch we do not want to increase the program counter further.
        if (!execute(inst))
            pc += 4; 
                        
    }

    printf("Execution Complete.\n");

    dump(output_path);

    printf("Registers: \n");
    print_reg(reg, REG_SIZE);

    // For comparing:

    /*printf("Correct Registers: \n");
    int32_t cd[REG_SIZE];
    load_dump(cd, REG_SIZE, "./tests/task4/t5.res");
    print_reg(cd, REG_SIZE);

    printf("Is the same?: %d \n", memcmp(reg, cd, REG_SIZE*sizeof(int32_t)) == 0);*/
    
    return 0;
}
