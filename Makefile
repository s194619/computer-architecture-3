#C compiler.
CC = gcc

#C compiler flags.
CFLAGS = -O3 -I inc/

#C source files.
SOURCES = $(shell find src/ -name '*.c')

#Output file.
OUTPUT = simulator

main: $(cfiles)
	$(CC) $(CFLAGS) $(SOURCES) -o $(OUTPUT)
clean: $(OUTPUT)
	rm $(OUTPUT)
.PHONY : clean